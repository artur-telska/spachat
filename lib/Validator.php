<?php
/**
 * @brief Class for data validation
 * Very basic implementation of data validation class
 * @author Artur Telska
 */
class Validator
{
    /**
     * Array for holding error messages
     * @var array $errorMessages
     */
    private $errorMessages = [];

    /**
     * Main validation method
     * @param  string  $name    name of field
     * @param  string  $data    data to validate
     * @param  array $conditions    validation conditions
     */
    public function validate($name, $data, $conditions)
    {
        foreach ($conditions as $key => $value) {
            $this->$key($name, $data, $value);
        }
    }

    /**
     * Check data for meeting minimal data length requirements
     * @param  string  $name    name of field
     * @param  string  $data    data to validate
     * @param  integer $minLength  minimal length of data
     */
    public function min($name, $data, $minLength)
    {
        if (strlen($data) < $minLength) {
            $this->errorMessages[] = "{$name} minimum length should be {$minLength} characters.";
        }
    }

    /**
     * Check data for meeting maximal data length requirements
     * @param  string  $name    name of field
     * @param  string  $data    data to validate
     * @param  integer $minLength  maximal length of data
     */
    public function max($name, $data, $maxLength)
    {
        if (strlen($data) > $maxLength) {
            $this->errorMessages[] = "{$name} maximum length should be {$maxLength} characters.";
        }
    }

    /**
     * Check data for meeting email requirements
     * @param  string  $name    name of field
     * @param  string  $data    data to validate
     */
    public function email($name, $data)
    {
        if (! filter_var($data, FILTER_VALIDATE_EMAIL)) {
            $this->errorMessages[] = "{$name} not meet email address requirements.";
        }
    }

    /**
     * Check is data is unique
     * @param  string  $name    name of field
     * @param  string  $data    data to validate
     * @param  string $value    database conditions in format "table|column"
     */
    public function unique($name, $data, $value)
    {
        $db = Registry::get('database');
        list($table, $column) = explode('|', $value);
        $query = $db->select($table, [$column])
                        ->where('email =', $data)
                        ->result('fetch');

        if (! empty($query)) {
            $this->errorMessages[] = "{$name} already exists.";
        }
    }

    /**
     * Return array with error messages
     * @return array error messages
     */
    public function getMessages()
    {
        return $this->errorMessages;
    }
}
