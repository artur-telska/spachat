<?php
/**
 * @brief Class for organize storage of data in session
 * Very basic implementation of session data stroage class
 * @author Artur Telska
 */
class Session
{
    /**
     * Constructor starts the session
     */
    public function __construct()
    {
        session_start();
    }

    /**
     * Stores the data in session using key
     * @param string $key   key
     * @param string $value data
     */
    public function set($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    /**
     * Get data from session by key
     * @param  key  $key     key
     * @return string      data
     */
    public function get($key)
    {
        return $_SESSION[$key];
    }

    /**
     * Store array of user data and set user login var
     * @param array $userData
     */
    public function setUserData($userData)
    {
        $_SESSION['user_data'] = serialize($userData);
        $_SESSION['user_logged_in'] = true;
    }

    /**
     * Get user data from session
     * @return array user data
     */
    public function getUserData()
    {
        return unserialize($_SESSION['user_data']);
    }

    /**
     * Destroy the session
     */
    public function destroy()
    {
        session_destroy();
    }

    /**
     * Check is user logged in
     * @return boolean
     */
    public function userLoggedIn()
    {
        return isset($_SESSION['user_logged_in']);
    }
}
