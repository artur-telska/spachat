<?php
class BaseController
{
    protected $viewName = null;
    protected $dataArray = null;

    public function __construct($actionName)
    {
        $this->{$actionName . '_'}();
    }

    public function render($viewName, $dataArray = false)
    {
        $this->viewName = $viewName;
        $this->dataArray = $dataArray;
    }

    public function getViewName()
    {
        return $this->viewName;
    }

    public function getDataArray()
    {
        return $this->dataArray;
    }

    public function redirect($location)
    {
        header("Location: http://localhost/spachat/public/?q={$location}");
        exit();
    }
}
