<?php
/**
 * @brief Class for working with POST and GET request data
 * Very simple class to get data from gloabal GET and POST arrays
 * @author Artur Telska
 * @package Agnesium
 */
class Request
{
    /**
     * Variable for holding action name, default action is "home"
     * @var string $actionName
     */
    private $actionName = 'home';

    /**
     * Variable for holding url segments data
     * @var array $urlSegments
     */
    private $urlSegments = [];

    /**
     * Constructor extracts data from request (GET) string and put it into
     * corresponding class properties
     */
    public function __construct()
    {
        if (! empty($_GET['q'])) {
            preg_match_all('/[a-z0-9-]+/', $_GET['q'], $segments);

            foreach ($segments[0] as $key => $value) {
                if ($key === 0) {
                    $value = str_replace('-', ' ', $value);
                    $value = ucwords($value);
                    $value = str_replace(' ', '', $value);
                    $value[0] = strtolower($value[0]);
                    $this->actionName = $value;
                } else {
                    $this->urlSegments[] = $value;
                }
            }
        }
    }

    /**
     * Returns requested action name
     * @return  string
     */
    public function getActionName()
    {
        return $this->actionName;
    }

    /**
     * Check if url have additional segments after action name
     * @return  boolean
     */
    public function haveUrlSegments()
    {
        return count($this->urlSegments) > 0 ? true : false;
    }

    /**
     * Return requested segment data, default segment is 0
     * @param   integer  $index  segment order number
     * @return  string
     */
    public function getUrlSegment($index = 0)
    {
        return $this->params[$index];
    }

    /**
     * Return array of all url segments data
     * @return array
     */
    public function getUrlSegments()
    {
        return $this->urlSegments;
    }

    /**
     * Check if request have post data and return true if it has
     * @return boolean
     */
    public function havePostData()
    {
        return count($_POST) > 0 ? true : false;
    }

    /**
     * Return post value of given key
     * @param   string $name key
     * @return  string
     */
    public function getPostValue($name)
    {
        return $_POST[$name];
    }

    /**
     * Return all post data
     * @return array
     */
    public function getPostValues()
    {
        return $_POST;
    }
}
