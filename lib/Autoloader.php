<?php
/**
 * @brief Class for autloading classes
 * Implementation of autloader for automatic load required classes
 * @author Artur Telska
 * @package Agnesium
 */
class Autoloader
{
    /**
     * Array of directories where looking for classes
     * @var array $dirs
     */
    private static $dirs = [
        'lib',
        'app',
    ];

    /**
     * Prints exception according to format
     * @param   string $className Name of class
     * @return  boolean
     */
    public static function loader($className)
    {
        foreach (self::$dirs as $dir) {
            $fileName = '../' . $dir . '/' . $className . '.php';
            $fileName = str_replace('/', DIRECTORY_SEPARATOR, $fileName);

            if (file_exists($fileName)) {
                require_once $fileName;

                if (class_exists($className)) {
                    return true;
                }
            }
        }

        return false;
    }
}

/**
 * Registering the autoloader
 */
spl_autoload_register('Autoloader::loader');
