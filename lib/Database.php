<?php
/**
 * @brief Class for interaction with database using PDO
 * Very basic implementation of database querying class
 * @author Artur Telska
 * @package Agnesium
 */
class Database
{
    /**
     * Contains connection string and its default value
     * @var string $conncetionString
     */
    protected $conncetionString = 'sqlite:../data/db.sqlite';

    /**
     * Contains query string
     * @var string $queryString
     */
    protected $queryString = null;

    /**
     * Reference to PDO object
     * @var object $pdo
     */
    protected $pdo = null;

    /**
     * Show exceptions or not
     * @var string $showExceptions
     */
    protected $showExceptions = true;

    /**
     * Initialize connection using $connectionString and create
     * new PDO object
     * @param string $connectionString connection string for
     * PDO e.g 'mysql:host=localhost;dbname=mydb'
     */
    public function __construct($connectionString = null)
    {
        if (! empty($connectionString)) {
            $this->connectionString = $connectionString;
        }

        try {
            $this->pdo = new PDO($this->conncetionString);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            $this->printException($e);
        }
    }

    /**
     * Prints exception according to format
     * @param  object $exception exception object
     */
    public function printException($exception)
    {
        if ($this->showExceptions) {
            echo "Database: {$exception->getMessage()} </br>";
        }
    }

    /**
     * Create new table in database
     * @param  string   $tableName    Name for new database table
     * @param  array    $tableParams   column names and data definitions
     * @return boolean/object
     */
    public function createTable($tableName, $tableParams)
    {
        foreach ($tableParams as $columnName => $dataType) {
            $tableColumns[] = $columnName . ' ' . $dataType;
        }

        $tableColumns = implode(', ', $tableColumns);
        $sql = "CREATE TABLE {$tableName} ({$tableColumns});";

        try{
            return $this->pdo->query($sql);
        } catch (PDOException $e) {
            $this->printException($e);
        }

        return false;
    }

    /**
     * Remove table from database
     * @param  string   $tableName  table name
     * @return boolean/object
     */
    public function dropTable($tableName)
    {
        $sql = "DROP TABLE {$tableName};";

        try {
            return $this->pdo->query($sql);
        } catch (PDOException $e) {
            $this->printException($e);
        }

        return false;
    }

    /**
     * Insert data into selected database table
     * @param  string   $tableName  table name
     * @param  array    $data       array of data for insert in table
     * @return boolean/object
     */
    public function insert($tableName, $data)
    {
        foreach ($data as $key => $value) {
            $keys[] = $key;
            $values[] = $this->pdo->quote($value);
        }

        $keysSection = implode(', ', $keys);
        $valuesSection = implode(', ', $values);
        $sql = "INSERT INTO {$tableName} ({$keysSection}) VALUES ({$valuesSection});";

        try {
            return $this->pdo->query($sql);
        } catch (PDOException $e) {
            $this->printException($e);
        }

        return false;
    }

    /**
     * Selects data by given criteria from given table
     * @param   string   $tableName  table name
     * @param   array    $columns    optional: array of column names
     * to select, if not set then select all columns
     * @return  object
     */
    public function select($tableName, $columns = false)
    {
        if ($columns) {
            $columnsSection = implode(', ', $columns);
            $sql = "SELECT {$columnsSection} FROM {$tableName} ";
        } else {
            $sql = "SELECT * FROM {$tableName} ";
        }

        $this->queryString = $sql;
        return $this;
    }

    /**
     * Add WHERE condition to $queryString
     * @param  string   $key    key for condition
     * @param  string   $value  condition value
     * @return object
     */
    public function where($key, $value)
    {
        $value = $this->pdo->quote($value);
        $sql = "WHERE {$key} {$value} ";
        $this->queryString .= $sql;
        return $this;
    }

    /**
     * Add AND condition to $queryString
     * @param  string   $key    key for condition
     * @param  string   $value  condition value
     * @return object
     */
    public function andWhere($key, $value)
    {
        $value = $this->pdo->quote($value);
        $sql = "AND {$key} {$value} ";
        $this->queryString .= $sql;
        return $this;
    }

    /**
     * Add OR condition to $queryString
     * @param  string   $key    key for condition
     * @param  string   $value  condition value
     * @return object
     */
    public function orWhere($key, $value)
    {
        $value = $this->pdo->quote($value);
        $sql = "OR {$key} {$value} ";
        $this->queryString .= $sql;
        return $this;
    }

    /**
     * Update row/rows in given table
     * @param  string   $tableName  table name
     * @param  array    $data       array with data to update
     * @return object
     */
    public function update($tableName, $data)
    {
        foreach ($data as $column => $value) {
            $setSection[] = $column . '=' . $this->pdo->quote($value);
        }

        $setSection = implode(', ', $setSection);
        $sql = "UPDATE {$tableName} SET {$setSection} ";
        $this->queryString = $sql;
        return $this;
    }

    /**
     * Delete data from given table
     * @param  string    $tableName     table name
     * @return object
     */
    public function delete($tableName)
    {
        $sql = "DELETE FROM {$tableName} ";
        $this->queryString = $sql;
        return $this;
    }

    /**
     * Fetch result of query string in object
     * @return object
     */
    public function result($mode = false)
    {
        $sql = $this->queryString . ';';
        //echo $sql. '<br>';

        try {
            $result = $this->pdo->query($sql);
            $result->setFetchMode(PDO::FETCH_OBJ);

            if ($mode === 'fetch') {
                return $result->fetch();
            }

            return $result;
        } catch (PDOException $e) {
            $this->printException($e);
        }

        return false;
    }
}
