<?php
/**
 * @brief Registry class for holding objects
 * Registry for store only one instance of class, the simple implementation
 * of Registry design pattern
 * @author Artur Telska
 * @package Agnesium
 */
class Registry
{
    /**
     * Holds refernces to all registered objects
     * @var array $register
     */
    private static $register = [];

    /**
     * Adds a new instance of class in register. If name parameter not set
     * then class name used as array key
     * @param object $item object to register
     * @param string $name name of object
     * @return object
     */
    public static function add(&$item, $name = null)
    {
        if (is_object($item) && is_null($name)) {
            $name = get_class($item);
        }

        $name = strtolower($name);
        self::$register[$name] = $item;
        return self::$register[$name];
    }

    /**
     * Returns refernce on object from register
     * @param  string object name
     * @return object
     */
    public static function &get($name)
    {
        $name = strtolower($name);

        if (array_key_exists($name, self::$register)) {
            return self::$register[$name];
        } else {
            $msg = "Registry: $name is not registred";
            throw new Exception($msg);
        }
    }

    /**
     * Returns true if object exists in register, and false otherwise
     * @param  string $name object name
     * @return boolean
     */
    public static function exists($name)
    {
        $name = strtolower($name);

        if (array_key_exists($name, self::$register)) {
            return true;
        }

        return false;
    }
}
