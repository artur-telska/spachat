<div class="uk-vertical-align uk-text-center uk-height-1-4">
    <div class="uk-vertical-align-middle" >
        <div class="uk-grid" data-uk-grid-margin="" style="padding-top:40%;">
            <div class="uk-width-medium-1-2">
                <form class="uk-panel uk-panel-box uk-form" method="post" action="?q=login">
                    <div class="uk-form-row">
                        <input class="uk-width-1-1 uk-form-large" type="text" name="email" placeholder="E-mail">
                    </div>
                    <div class="uk-form-row">
                        <input class="uk-width-1-1 uk-form-large" type="text" name="password" placeholder="Password">
                    </div>
                    <div class="uk-form-row">
                        <button type="submit" class="uk-width-1-1 uk-button uk-button-primary uk-button-large">Login</button>
                    </div>
                    <div class="uk-form-row uk-text-small">
                        <label class="uk-float-left"><input type="checkbox"> Remember Me</label>
                        <a class="uk-float-right uk-link uk-link-muted" href="#">Forgot Password?</a>
                    </div>
                </form>
            </div>
            <div class="uk-width-medium-1-2">
                <form class="uk-panel uk-panel-box uk-form" method="post" action="?q=signup">
                    <?php if (! empty($dataArray['error_messages'])): ?>
                    <div class="uk-form-row">
                        <?php foreach ($dataArray['error_messages'] as $error): ?>
                            <b><?= $error;?></b></br>
                        <?php endforeach; ?>
                    </div>
                    <?php endif; ?>
                    <div class="uk-form-row">
                        <input class="uk-width-1-1 uk-form-large" type="text" name="email" placeholder="E-mail">
                    </div>
                    <div class="uk-form-row">
                        <input class="uk-width-1-1 uk-form-large" type="text" name="password" placeholder="Password">
                    </div>
                    <div class="uk-form-row">
                        <input class="uk-width-1-1 uk-form-large" type="text" name="password_repeat" placeholder="Repeat password">
                    </div>
                    <div class="uk-form-row">
                        <button type="submit" class="uk-width-1-1 uk-button uk-button-primary uk-button-large">Sign Up</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
