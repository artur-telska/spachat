<!DOCTYPE html>
<html lang="en-gb" dir="ltr" class="uk-height-1-1">
    <head>
        <meta charset="utf-8">
        <title>Login layout example - UIkit documentation</title>
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
        <link rel="apple-touch-icon-precomposed" href="images/apple-touch-icon.png">
        <link rel="stylesheet" href="css/uikit.min.css">
        <script src="../vendor/jquery.js"></script>
        <script src="js/uikit.min.js"></script>
    </head>
     <body class="uk-height-1-1">

         <?php include "{$viewName}.php";?>

    </body>
</html>
