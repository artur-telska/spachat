<?php
class AppController extends BaseController
{
    public function __construct($actionName)
    {
        parent::__construct($actionName);
    }

    public function home_()
    {
        $this->render('sign_up_and_login_form');
    }

    public function signUp_()
    {
        $request = Registry::get('request');

        if ($request->havePostData()) {
            $email = $request->getPostValue('email');
            $password = $request->getPostValue('password');

            $validator = Registry::get('validator');

            $validator->validate('Email', $email, [
                'email' => true,
                'min' => 6,
                'max' => 200,
                'unique' => 'users|email'
                ]);

            $validator->validate('Password', $password, [
                'min' => 8,
                'max' => 200
                ]);

            $errorMessages = $validator->getMessages();

            if (count($errorMessages) > 0) {
                $data['error_messages'] = $errorMessages;
                $this->render('sign_up_and_login_form', $data);
            } else {

                $db = Registry::get('database');

                $db->insert('users',[
                    'email' => $email,
                    'password' => $password
                    ]);

                $session = Registry::get('session');

                $session->setUserData([
                    'email' => $email,
                    ]);

                $this->redirect('chat');
            }

        } else {
            $this->redirect('home');
        }
    }

    public function login_()
    {
        $request = Registry::get('request');
        $email = $request->getPostValue('email');
        $password = $request->getPostValue('password');

        $validator = Registry::get('validator');

        $validator->validate('Email', $email, [
            'email' => true,
            'min' => 6,
            'max' => 200
            ]);

        $validator->validate('Password', $password, [
            'min' => 8,
            'max' => 200
            ]);

        $errorMessages = $validator->getMessages();

        if (count($errorMessages) > 0) {
            $this->redirect('home');
        } else {
            $db = Registry::get('database');

            $query = $db->select('users')
                        ->where('password =', $password)
                        ->andWhere('email = ', $email)
                        ->result('fetch');

            if (is_object($query)) {
                $session = Registry::get('session');
                $session->setUserData($query);
                $this->redirect('chat');
            } else {
                $this->redirect('home');
            }
        }
    }

    public function logout_()
    {
        $session = Registry::get('session');
        $session->destroy();
        $this->redirect('home');
    }

    public function chat_()
    {
        $session = Registry::get('session');

        if ($session->userLoggedIn()) {
            $userData = $session->getUserData();
            echo $userData->email;

            $this->render('chat');

        } else {
            $this->redirect('home');
        }
    }

    public function postMessage_()
    {
        $request = Registry::get('request');
        $db = Registry::get('database');
        $userData = Registry::get('session')->getUserData();
        $message = $request->getPostValue('message');

        if (! empty($message)) {
            $db->insert('messages', [
                'sender_id' => $userData->id,
                'message' => $message
                ]);
        }
    }

    public function getMessages_()
    {
        $db = Registry::get('database');

        $data['messages'] = $db->select('messages_senders')->result();

        $this->render('chat', $data);

    }

}
