CREATE TABLE "main"."users" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    "email" TEXT NOT NULL UNIQUE,
    "password" TEXT NOT NULL,
    "is_active" INTEGER NOT NULL DEFAULT (0),
    "updated_on" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "created_on" TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TRIGGER "main"."timestamp_on_update"
AFTER UPDATE
ON users
FOR EACH ROW
BEGIN
	UPDATE users
	SET updated_on = CURRENT_TIMESTAMP
	WHERE  id = NEW.id;
END;

CREATE TABLE "main"."messages" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    "sender_id" TEXT NOT NULL,
    "message" TEXT NOT NULL,
    "created_on" TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE VIEW messages_senders AS
SELECT users.email, messages.created_on, messages.message
FROM messages
JOIN users
ON users.id =  messages.sender_id
ORDER BY messages.created_on ASC;
