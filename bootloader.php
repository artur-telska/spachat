<?php
require 'lib/Autoloader.php';

Registry::add(new Session());
Registry::add(new Request());
Registry::add(new Database());
Registry::add(new Validator());

$app = new AppController(Registry::get('request')->getActionName());


call_user_func(function($viewName, $dataArray) {
    require "app/views/layout.php";
},$app->getViewName(), $app->getDataArray());
